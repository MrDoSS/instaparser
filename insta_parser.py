import logging
from itertools import cycle
from json import JSONEncoder
from instagram import Account, Media, AsyncWebAgentAccount, AsyncWebAgent, exceptions
from aiohttp import ClientProxyConnectionError

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(filename='parser.log', level=logging.INFO, format=LOG_FORMAT)
LOGGER = logging.getLogger(__name__)

class InstaParser:
    class TYPE:
        AUTHORIZED = 'AUTHORIZED'
        UNAUTHORIZED = 'UNAUTHORIZED'

    def __init__(self, proxy_pool):
        self.type = InstaParser.TYPE.UNAUTHORIZED
        self.proxy_pool = proxy_pool

    async def __await__(self):
        pass

    async def __aiter__(self):
        return self

    async def __aenter__(self):
        await self._set_agent()
        await self._set_proxy()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.agent.delete()

    async def _set_proxy(self):
        try:
            self.proxy = next(self.proxy_pool)
            LOGGER.info('Attempt to set proxy: %s', self.proxy)
            await self.agent.update(settings={'proxy': 'http://{0}'.format(self.proxy), 'timeout': 30})
            LOGGER.info('Success')
        except ClientProxyConnectionError:
            LOGGER.warning('Bad proxy: %s', self.proxy)
            await self._set_proxy()

    async def _set_agent(self):
        # TODO: Сделать авторизованого клиента
        username = password = False
        if username and password:
            self.agent = AsyncWebAgentAccount(username)
            await self.agent.auth(password)
            self.type = Parser.TYPE.AUTHORIZED
        else:
            self.agent = AsyncWebAgent()

    async def get_all_media(self, username, pointer=None):
        result = []

        LOGGER.info('%s parsing medias from %s', 'Start' if pointer is None else 'Continue', username)

        try:
            account = Account(username)

            if pointer is None:
                medias, pointer = await self.agent.get_media(account, count=50)

                for media in medias:
                    result.append(media)

            while not pointer is None:
                medias, pointer = await self.agent.get_media(account, pointer=pointer, count=50)

                for media in medias:
                    result.append(media)
                
        except exceptions.UnexpectedResponse as e:
            LOGGER.warning('Needed change proxy')
            await self._set_proxy()
            await self.get_all_media(username, pointer)

        LOGGER.info('Parsed %i medias from %s', len(result), username)
        return result

    @staticmethod
    def media_to_dict(medias):
        result = []
        for media in medias:
            d = {}
            for key in ['code', 'caption', 'likes_count', 'display_url', 'date']:
                d[key] = getattr(media, key)
            d['owner'] = str(media.owner)
            result.append(d)
        return result
    
    
        