import asyncio
import aio_pika
import logging
import json

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(filename='rabbit.log', level=logging.INFO, format=LOG_FORMAT)
LOGGER = logging.getLogger(__name__)

class RabbitHelper:
    def __init__(self, loop, on_message, url='amqp://guest:guest@127.0.0.1/'):
        self.loop = loop
        self.url = url
        self.request_queue_name = "test_queue_request"
        self.response_queue_name = "test_queue_response"
        self.on_message = on_message

    async def run(self):
        await self._connect()
        await self._create_channel()
        await self._set_qos()
        await self._declare_queue()
        LOGGER.info('Running')

    async def stop(self):
        await self.connection.close()
        LOGGER.info('Stopping')

    async def _connect(self):
        self.connection = await aio_pika.connect_robust(self.url, loop=self.loop)
        LOGGER.info('Connecting')

    async def _create_channel(self):
        self.channel = await self.connection.channel()
        LOGGER.info('Creating channel')

    async def _set_qos(self, prefetch_count=100):
        await self.channel.set_qos(prefetch_count=prefetch_count)
        LOGGER.info('Setting QOS')

    async def _declare_queue(self):
        self.queue = await self.channel.declare_queue(self.request_queue_name, durable=True)
        await self.queue.consume(self._on_message)
        LOGGER.info('Declaring queue')

    async def _on_message(self, message: aio_pika.IncomingMessage):
        LOGGER.info('Received message: %s', message.body)
        try:
            res = await self.on_message(message.body)
            await message.ack()
            await self.send_message(res)
        except Exception as e:
            LOGGER.error('Error: %s', e)
            await message.reject()
        await asyncio.sleep(1)

    async def send_message(self, message):
        await self.channel.default_exchange.publish(
            aio_pika.Message(body=json.dumps(message).encode()),
            routing_key=self.response_queue_name
        )
        LOGGER.info('Sended message')