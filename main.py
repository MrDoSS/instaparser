import asyncio
import json
import logging
import requests
from itertools import cycle
from lxml.html import fromstring
from rabbit_helper import RabbitHelper
from insta_parser import InstaParser

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
logging.basicConfig(filename='main.log', level=logging.INFO, format=LOG_FORMAT)
LOGGER = logging.getLogger(__name__)

class Server:
    def __init__(self, loop):
        self.loop = loop
        self._get_proxies()

    def _get_proxies(self):
        url = 'https://free-proxy-list.net/'
        response = requests.get(url)
        parser = fromstring(response.text)
        proxies = set()
        for i in parser.xpath('//tbody/tr'):
            if i.xpath('.//td[5][contains(text(),"elite proxy")]') and i.xpath('.//td[3][contains(text(),"RU")]'):
                proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
                proxies.add(proxy)
        self.proxies = proxies
        self.proxy_pool = cycle(self.proxies)
        LOGGER.info('Parsed %i proxies', len(proxies))

    async def run(self):
        self.rh = RabbitHelper(self.loop, self.on_message)
        await self.rh.run()

    async def on_message(self, message):
        json_message = json.loads(message)
        task_id = json_message.get('task_id', None)
        username = json_message.get('username', False)

        result = {
            'task_id': task_id,
            'success': False
        }

        if username:
            async with InstaParser(self.proxy_pool) as parser:
                medias = await parser.get_all_media(username)

                if medias:
                    result['goods'] = InstaParser.media_to_dict(medias)
                    result['success'] = True

        return json.dumps(result, sort_keys=True)

async def main(loop):
    server = Server(loop)
    await server.run()
    return server

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    m = loop.run_until_complete(main(loop))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(m.rh.stop())
        pass